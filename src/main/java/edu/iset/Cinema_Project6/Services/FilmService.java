package edu.iset.Cinema_Project6.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iset.Cinema_Project6.Repository.FilmRepository;
import edu.iset.Cinema_Project6.model.Film;

@Service
public class FilmService   {

	@Autowired	 
    private FilmRepository filmRepo;
	
	public List<Film> getAllFilm() {
		
		return filmRepo.findAll();
	}

	public Film addFilm(Film film) {
		if(filmRepo.findByTitle(film.getTitle())==null)	
    	{
    		return filmRepo.save(film);
    		}
		
		return null;
	}

}