package edu.iset.Cinema_Project6.Services;

import java.util.List;

import edu.iset.Cinema_Project6.model.Film;

public interface FilmServices  {
	public List<Film> getAllFilm();
	public Film addFilm(Film film);

}