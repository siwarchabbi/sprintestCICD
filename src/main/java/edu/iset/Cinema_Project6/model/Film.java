package edu.iset.Cinema_Project6.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.GenerationType;

@Entity
public class Film implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	private String title;
	private String price;
	private String genre;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Film(Long id, String title, String price, String genre) {
		super();
		Id = id;
		this.title = title;
		this.price = price;
		this.genre = genre;
	}
	@Override
	public String toString() {
		return "Film [Id=" + Id + ", title=" + title + ", price=" + price + ", genre=" + genre + "]";
	}
	public Film() {
		
	}



}