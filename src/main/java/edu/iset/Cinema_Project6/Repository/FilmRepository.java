package edu.iset.Cinema_Project6.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.iset.Cinema_Project6.model.Film;

public interface FilmRepository extends JpaRepository<Film, Long> {
	Film  findByTitle(String title);

}