package edu.iset.Cinema_Project6.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.iset.Cinema_Project6.Services.FilmService;
import edu.iset.Cinema_Project6.model.Film;

@RestController
@RequestMapping("/film")
public class FilmController {
	@Autowired
	FilmService filmService ;

	@GetMapping
	public List<Film> getAllFilm() {
		return filmService.getAllFilm();
	}
	

	@PostMapping
	public Film addFilm(@RequestBody Film film) {
		return filmService.addFilm(film);
	}
}